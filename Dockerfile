FROM node:latest as node
RUN mkdir -p /app
WORKDIR /app
COPY package*.json /app/
RUN npm install
RUN pwd 
COPY . /app/

FROM nginx:1.17.1-alpine
RUN pwd
COPY /dist/test-project /usr/share/nginx/html
#EXPOSE 4200
#CMD ["npm", "run", "start"]
